#!/bin/bash
source venv/bin/activate

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z "$SQL_HOST" "$SQL_PORT"; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

flask deploy
sleep 10
flask run -h 0.0.0.0

exec "$@"
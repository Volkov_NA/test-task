from flask_migrate import Migrate
from flask_migrate import upgrade
from src import create_app, db

from src.models.events import Event
from src.models.users import User

app = create_app()
migrate = Migrate(app, db)


@app.shell_context_processor
def make_shell_context():
    return dict(db=db, Event=Event, User=User)


@app.cli.command()
def deploy():
    upgrade()

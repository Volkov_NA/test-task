from flask import Flask

from .config import Config
from .extensions import db
from .extensions import jwt


def create_app():
    app = Flask('Events app')
    app.config.from_object(Config)

    register_extensions(app)

    from .api import api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    from .auth import auth_bp
    app.register_blueprint(auth_bp)

    return app


def register_extensions(app):
    db.init_app(app)
    jwt.init_app(app)


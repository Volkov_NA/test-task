import uuid

from sqlalchemy_utils import EmailType
from sqlalchemy_utils import PasswordType
from sqlalchemy.dialects.postgresql import UUID

from src import db


class User(db.Model):
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    email = db.Column(EmailType)
    password = db.Column(PasswordType, unique=False, nullable=False)

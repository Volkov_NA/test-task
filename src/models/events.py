import uuid

from sqlalchemy.sql import func
from sqlalchemy.dialects.postgresql import UUID

from src import db


class Event(db.Model):
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    start_at = db.Column(db.DateTime, nullable=False)
    finish_at = db.Column(db.DateTime, nullable=False)
    created_at = db.Column(db.DateTime, server_default=func.now())
    updated_at = db.Column(db.DateTime, onupdate=func.now())
    theme = db.Column(db.String(120), nullable=False)

import os


class Config(object):
    SQLALCHEMY_DATABASE_URI = 'postgresql://{}:{}@{}/{}'.format(
        os.getenv('POSTGRES_USER'),
        os.getenv('POSTGRES_PASSWORD'),
        os.getenv('POSTGRES_HOST'),
        os.getenv('POSTGRES_DB'))

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    DEBUG = os.getenv('DEBUG', False)
    FLASK_ENV = os.getenv('FLASK_ENV', 'production')
    FLASK_MODE = os.getenv('FLASK_MODE', 'production')
    SECRET_KEY = os.getenv('SECRET_KEY', 'super-secret')
    JWT_AUTH_USERNAME_KEY = os.getenv('JWT_AUTH_USERNAME_KEY', 'email')


class ProdConfig(Config):
    DEBUG = False
    FLASK_MODE = 'production'
    FLASK_ENV = 'production'

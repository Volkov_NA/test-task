import re

from flask_restx import Resource

from .controller import AuthController
from .schema import AuthSchema

api = AuthSchema.api


def validate_user_data(data):
    email = data.get('email')
    password = data.get('password')

    email_pattern = re.compile(r'(^|\s)[-a-z0-9_.]+@([-a-z0-9]+\.)+[a-z]{2,6}(\s|$)')

    if not email_pattern.match(email):
        return False, 'Wrong email'

    if len(password) < 6:
        return False, 'Password is too short (minimal length 6)'

    if len(password) > 24:
        return False, 'Password is too long (maximum length 24)'

    return True, None


@api.route('/login')
class AuthLogin(Resource):
    @api.doc(
        'Auth login',
        responses={
            200: ('Logged in', AuthSchema.auth_success),
            400: 'Validations failed.',
            403: 'Incorrect password or incomplete credentials.',
            404: 'Email does not match any account.',
        },
    )
    @api.expect(AuthSchema.auth_login, validate=True)
    def post(self):
        login_data = self.api.payload
        is_valid, message = validate_user_data(login_data)

        if is_valid:
            token = AuthController.login(login_data)
            if token:
                return token
            api.abort(403, 'Wrong email or password')
        self.api.abort(400, message)


@api.route('/register')
class AuthRegister(Resource):
    @api.doc(
        'Auth registration',
        responses={
            201: 'Successfully registered user.',
            400: 'Validations failed.',
            403: 'User already exist',
        },
    )
    @api.expect(AuthSchema.auth_register, validate=True)
    def post(self):
        register_data = self.api.payload
        is_valid, message = validate_user_data(register_data)

        if is_valid:
            registered_email = AuthController.register(register_data)

            if registered_email:
                return {'message': f'{registered_email} register success.'}
            self.api.abort(403, 'User already exist')
        self.api.abort(400, message)

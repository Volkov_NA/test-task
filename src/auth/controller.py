from flask_jwt_extended import create_access_token

from src import db
from src.models.users import User


class AuthController:
    @staticmethod
    def register(data):
        email = data.get('email')
        password = data.get('password')

        if User.query.filter_by(email=email).first() is not None:
            return None

        user = User(
            email=email,
            password=password
        )
        db.session.add()
        db.session.commit()

        return user.email

    @staticmethod
    def login(data):
        email = data.get('email')
        password = data.get('password')

        user = User.query.filter_by(email=email).first()

        if not user:
            return None
        # Выглядит так себе, но это сравнение хэшей, см. поле password в модели User
        elif password == user.password:
            return create_access_token(identity=user.id)
        return None

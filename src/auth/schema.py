from flask_restx import Namespace, fields


class AuthSchema:
    api = Namespace('auth', description='Authenticate and receive tokens.')

    auth_login = api.model(
        'Login data',
        {
            'email': fields.String(required=True),
            'password': fields.String(required=True),
        },
    )

    auth_register = api.model(
        'Registration data',
        {
            'email': fields.String(required=True),
            'password': fields.String(required=True),
        },
    )

    auth_success = api.model(
        'Auth success response',
        {
            'access_token': fields.String,
        },
    )

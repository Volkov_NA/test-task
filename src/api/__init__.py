from flask import Blueprint
from flask_restx import Api

from .events.view import api as events_namespace

api_bp = Blueprint('api', __name__)

api = Api(api_bp, title='Event service API')

api.add_namespace(events_namespace)

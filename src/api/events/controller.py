from sqlalchemy import and_

from src.models.events import Event
from src import db


class EventsController:
    @staticmethod
    def get_event_data(event_id):
        return Event.query.get(event_id)

    @staticmethod
    def get_events_list(filters={}):
        event_query = db.session.query(Event)

        theme_filter = filters.get('theme')
        if theme_filter:
            event_query = event_query.filter(Event.theme == theme_filter)

        date_filter = filters.get('date')
        if date_filter:
            event_query = event_query.filter(
                and_(
                    Event.start_at <= date_filter,
                    Event.finish_at >= date_filter,
                )
            )
        print(filters)
        return event_query.all()

    @staticmethod
    def create_event(data):
        event = Event(**data)
        db.session.add(event)
        db.session.commit()

        return event

    @staticmethod
    def update_event(event_id, data):
        event = Event.query.get(event_id).update(**data)
        db.session.commit()

        return event

    @staticmethod
    def delete_event(event_id):
        event = Event.query.get(event_id)

        db.session.delete(event)
        db.session.commit()

        return event is not None

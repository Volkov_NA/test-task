from flask_restx import Namespace
from flask_restx import fields
from flask_restx import inputs

from .constants import THEMES_LIST


class EventsSchema:
    api = Namespace('events', description='Events CRUD schemas')
    create_update_event = api.model(
        'Event changing data', {
            'theme': fields.String(required=True, enum=THEMES_LIST),
            'start_at': fields.DateTime(required=True),
            'finish_at': fields.DateTime(required=True),
        }
    )
    receive_event = api.clone(
        'Event received data', create_update_event, {
            'id': fields.String(readonly=True),
            'created_at': fields.String(readonly=True),
            'updated_at': fields.String(readonly=True),
        }
    )
    event_list = api.model(
        'Events list', {
            'events': fields.List(fields.Nested(receive_event)),
        }
    )
    filters_parser = api.parser()
    filters_parser.add_argument('theme', choices=THEMES_LIST, help='Bad choice: {error_msg}', location='args')
    filters_parser.add_argument('date', type=inputs.datetime_from_iso8601, location='args')
    filters_parser.add_argument('save', type=inputs.boolean, location='args')

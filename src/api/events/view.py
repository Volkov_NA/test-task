from flask_restx import Resource
from flask_jwt_extended import jwt_required

from .controller import EventsController
from .schema import EventsSchema

api = EventsSchema.api


@api.route('/')
class EventView(Resource):
    @api.doc(
        'Get events',
        responses={
            200: ('Received', EventsSchema.event_list),
            400: 'Wrong filter',
        },
    )
    @api.expect(EventsSchema.filters_parser)
    @api.marshal_with(EventsSchema.event_list)
    def get(self):
        args = EventsSchema.filters_parser.parse_args()
        filters = {
            'theme': args.get('theme'),
            'date': args.get('date'),
        }
        return {
            'events': EventsController.get_events_list(filters)
        }

    @api.doc(
        'Create event',
        responses={
            201: ('Created', EventsSchema.receive_event),
            400: 'Validation failed',
        }
    )
    @api.expect(EventsSchema.create_update_event, validate=True)
    @api.marshal_with(EventsSchema.receive_event)
    @jwt_required
    def post(self):
        data = self.api.payload

        if data['start_at'] >= data['finish_at']:
            self.api.abort(400, 'Validation error: start_at must be less, then finish_at')

        return EventsController.create_event(data)


@api.route('/<uuid:event_id>')
class EventView(Resource):
    @api.doc(
        'Get event',
        responses={
            200: ('Received', EventsSchema.receive_event),
            404: 'Event not found',
        },
    )
    @api.marshal_with(EventsSchema.receive_event)
    def get(self, event_id):
        event = EventsController.get_event_data(event_id)
        if event:
            return event, 200
        self.api.abort(404, 'Event not found')

    @api.doc(
        'Update event',
        responses={
            201: ('Updated', EventsSchema.receive_event),
            400: 'Validation failed',
            404: 'Event not found'
        }
    )
    @api.expect(EventsSchema.create_update_event, validate=True)
    @api.marshal_with(EventsSchema.receive_event)
    @jwt_required
    def put(self, event_id):
        data = self.api.payload

        if data['start_at'] >= data['finish_at']:
            self.api.abort(400, 'Validation error: start_at must be less, then finish_at')

        return EventsController.update_event(event_id, data)

    @api.doc(
        'Delete event',
        responses={
            204: 'Deleted',
            404: 'Event not found',
        },
    )
    @api.marshal_with(EventsSchema.receive_event)
    def delete(self, event_id):
        deleted = EventsController.delete_event(event_id)
        if deleted:
            return 'Deleted', 204
        self.api.abort(400, 'Event not found')

FROM python:3.7

ENV FLASK_APP main.py

RUN useradd -ms /bin/bash event_user
USER event_user

WORKDIR /app

COPY requirements/requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt

COPY main.py entrypoint.sh ./

ENTRYPOINT ["./entrypoint.sh"]